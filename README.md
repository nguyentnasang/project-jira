<!-- # 1. Bạn mentor được các giờ nào trong tuần:

Các buổi trong tuần từ t2 – t7 (bđ từ 18h30 – 21h30).

CN: Sáng 8h30 -> 11h30 và 13h00 -> 17 h30

# 2. Trong 1 ngày bạn dành ra bao nhiêu thời gian để check ib của học viên khi các bạn đó có thắc mắc ib trên group hoặc inbox riêng.

em có thể dành khoản 2 - 3 tiếng mỗi ngày để hổ trợ. Khung giờ 9h-12h là thời điểm mà em hổ trợ, em cũng có thể dành thêm thời gian để hổ trợ khi rãnh rỗi.

# 3.Nếu nhận được 1 lỗi phát sinh 500 error server bạn sẽ xử lý như thế nào khi gặp những câu hỏi từ học viên, liệt kê các bước bạn sẽ xử lý.

B1: kiểm tra lỗi tìm hiểu xem lỗi gì đang diễn ra, hướng dẫn học viên xác định lỗi ví dụ như xem thông tin lỗi ở console.log.

B2: sau khi đã xác định được lỗi hổ trợ sinh viên về quá trình giải quyết, nếu liên quan đến code thì giúp học viên sửa lại code.

B3: sau khi fix lỗi xong kiểm tra lại xem đã giải quyết hết chưa, đảm bảo web hoạt động bình thường.

B4: hỏi học viên có còn thắc mắt về vấn đề đó không, nếu còn thì giải thích tường minh hơn cho học viên.

# 4.Nếu được sắp vào khung giờ cố định thì công việc chính của bạn có ảnh hưởng đến việc mentor hay không ví dụ như OT ...

Hiện tại thì em không ảnh hưởng gì với khung giờ cố định về việc mentor.

# 5. Tại sao bạn apply công việc này ?

Em muốn có cơ hội học hỏi thêm từ các giảng viên người có nhiều năm kinh nghiệm trong nghề giống như anh Sĩ vậy, làm quen với nhiều phương pháp mới để cải thiện kỹ năng. Em khá là ít nói công việc mentor sẽ giúp em thêm các mối quan hệ trong nghành.
 -->
