import { createSlice } from "@reduxjs/toolkit";
import { idProjLocalService, userLocalService } from "../service/localService";

const initialState = {
  user: userLocalService.get(),
  idProj: idProjLocalService.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfo: (state, action) => {
      state.user = action.payload;
    },
    setIdPoject: (state, action) => {
      state.idProj = action.payload;
    },
  },
});

export const { setUserInfo, setIdPoject } = userSlice.actions;

export default userSlice.reducer;
