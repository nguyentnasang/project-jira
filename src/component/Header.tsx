import React from "react";
import {
  HomeOutlined,
  AppstoreAddOutlined,
  FolderAddOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { NavLink } from "react-router-dom";
export default function Header() {
  return (
    <div className="flex justify-between items-center px-20 py-5 border-b-2 border-neutral-600">
      <div className="animate-bounce font-bold text-5xl text-gray-500">
        JIRA
      </div>
      <ul className="grid grid-cols-4 font-medium gap-5">
        <NavLink to={"/home"}>
          <li className="flex items-center justify-center text-gray-500 hover:text-gray-900 cursor-pointer">
            <HomeOutlined className="mr-2" /> HOME
          </li>
        </NavLink>
        <NavLink to={"/createproject"}>
          <li className="flex items-center justify-center text-gray-500 hover:text-gray-900 cursor-pointer uppercase">
            <FolderAddOutlined className="mr-2" />
            create project
          </li>
        </NavLink>
        <NavLink to={"/createtask"}>
          <li className="flex items-center justify-center text-gray-500 hover:text-gray-900 cursor-pointer uppercase">
            <AppstoreAddOutlined className="mr-2" />
            create task
          </li>
        </NavLink>
        <NavLink to={"/"}>
          <li className="flex items-center justify-center text-gray-500 hover:text-gray-900 cursor-pointer">
            <LogoutOutlined className="mr-2" />
            LOGOUT
          </li>
        </NavLink>
      </ul>
    </div>
  );
}
