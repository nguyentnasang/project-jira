import React, { useState } from "react";
import { useEffect } from "react";
import {
  getProjectDetail,
  getTaskDetail,
  deleteTask,
} from "./../../service/projectService";
import { NavLink, useParams } from "react-router-dom";

import { idProjLocalService } from "../../service/localService";
import { getUserbyProject } from "../../service/userService";
import AssignMember from "./AssignMember";
import { DeleteOutlined } from "@ant-design/icons";
import { message } from "antd";

export default function TaskPage() {
  let { id } = useParams();
  idProjLocalService.set(id);
  let [isLoad, setIsLoad] = useState(true);
  let [dataArr, setdataArr] = useState<any>();
  let renderDataArr = dataArr?.map((item: any) => {
    return (
      <div>
        <h2 className="border-2 border-slate-500 rounded-xl ">
          {item.statusName}
        </h2>
        <div className="flex flex-col">
          {item.lstTaskDeTail.map((item: any) => {
            return (
              <div className="w-full relative">
                <button
                  onClick={() => {
                    deleteTask(item.taskId)
                      .then((res) => {
                        message.success("xóa thành công");
                        setIsLoad(!isLoad);
                      })
                      .catch((err) => {
                        console.log(err);
                        message.error("xóa thất bại");
                      });
                  }}
                  className="absolute hover:bg-red-400 top-5 right-5 bg-red-500 px-2 text-white py-1 rounded-full"
                >
                  <DeleteOutlined />
                </button>
                <NavLink to={`/taskdetail/${item.taskId}`}>
                  <button
                    className="bg-slate-300 rounded-xl my-2 py-5 w-full text-neutral-500"
                    onClick={() => {
                      getTaskDetail(item.taskId)
                        .then((res) => {
                          console.log(res);
                        })
                        .catch((err) => {
                          console.log(err);
                        });
                    }}
                  >
                    <p className="font-semibold text-lg">{item.taskName}</p>
                    <p className="">{item.priorityTask.priority}</p>
                  </button>
                </NavLink>
              </div>
            );
          })}
        </div>
      </div>
    );
  });
  useEffect(() => {
    getProjectDetail(id)
      .then((res) => {
        let { lstTask } = res.data.content;
        setdataArr(lstTask);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [isLoad]);

  return (
    <div className="p-5">
      <AssignMember />
      <div className="grid grid-cols-4 gap-1">{renderDataArr}</div>
    </div>
  );
}
