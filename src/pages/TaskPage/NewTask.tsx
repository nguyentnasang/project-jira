import React, { useState } from "react";
import { Button, Checkbox, Form, Input, message, Modal, Select } from "antd";
import { Option } from "antd/es/mentions";
import { idProjLocalService } from "../../service/localService";
import { postCreareTask } from "./../../service/projectService";

export default function NewTask() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const onFinish = (values: any) => {
    // console.log("Success:", values);
    let data = {
      description: "",
      listUserAsign: undefined,
      originalEstimate: 1,
      priorityId: 1,
      projectId: idProjLocalService.get() * 1,
      statusId: "1",
      taskName: values.taskName,
      timeTrackingRemaining: 1,
      timeTrackingSpent: 1,
      typeId: 1,
    };
    console.log("datalol", data);
    postCreareTask(data)
      .then((res) => {
        console.log(res);
        message.success("thêm thành công");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("thêm thất bại");
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div>
      <Button className="bg-blue-500" onClick={showModal}>
        Add task
      </Button>
      <Modal
        title="Add Task"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          layout="vertical"
          name="basic"
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Task name"
            name="taskName"
            rules={[{ message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item className="flex justify-center items-center">
            <Button className="bg-blue-500" htmlType="submit">
              add task
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
