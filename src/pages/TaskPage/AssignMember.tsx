import React, { useEffect, useState } from "react";
import { Button, Checkbox, Form, Input, message, Select, Modal } from "antd";
import {
  getUser,
  getUserbyProject,
  postRemoteUserProj,
} from "../../service/userService";
import { postAddUserProj } from "./../../service/userService";
import { idProjLocalService } from "../../service/localService";
import { NavLink } from "react-router-dom";
import { DeleteOutlined } from "@ant-design/icons";
import NewTask from "./NewTask";
export default function AssignMember() {
  const [isAdded, setIsAdded] = useState(false);
  let [userArr, setUserArr] = useState([{ name: "", userId: 0 }]);

  const onFinish = (values: any) => {
    console.log("Success:", values);
    let dataArr = { ...values, projectId: idProjLocalService.get() * 1 };
    console.log("dataArr", dataArr);
    postAddUserProj(dataArr)
      .then((res) => {
        console.log(res);
        setIsAdded(!isAdded);
        message.success("thêm thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("thêm thất bại");
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  const { Option } = Select;
  useEffect(() => {
    getUser()
      .then((res) => {
        setUserArr(res.data.content);
      })
      .catch((err) => {});
    getUserbyProject(idProjLocalService.get() * 1)
      .then((res) => {
        console.log("res", res);
        newUserArrs(res.data.content);
      })
      .catch((err) => {
        console.log("err", err);
        newUserArrs([]);
      });
  }, [isAdded]);
  let renderUserArr = userArr?.map((item) => {
    return <Option value={item.userId}>{item.name}</Option>;
  });
  let [userArrs, newUserArrs] = useState([{ name: "", userId: 0 }]);

  let renderuserArrs = userArrs.map((item, index) => {
    return item.name ? (
      <div className="flex justify-between items-center m-5">
        <p>{`${index + 1}.  ${item.name}`} </p>
        <button
          onClick={() => {
            postRemoteUserProj({
              projectId: idProjLocalService.get() * 1,
              userId: item.userId,
            })
              .then((res) => {
                message.success("xóa thành công");
                setIsAdded(!isAdded);
                console.log(res);
              })
              .catch((err) => {
                console.log(err);
                console.log({
                  projectId: idProjLocalService.get() * 1,
                  userId: item.userId,
                });
                message.error("xóa thất bại");
              });
          }}
          className="bg-red-500 text-white px-2 py-1 rounded-md ml-5"
        >
          <DeleteOutlined />
        </button>
      </div>
    ) : (
      <></>
    );
  });
  return (
    <div className="flex justify-center items-center flex-col">
      {" "}
      <Form
        className=" w-full mt-10"
        name="basic"
        style={{ maxWidth: 500 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item name="userId" rules={[{ required: true }]}>
          <Select placeholder="Thêm người dùng vào dự án" allowClear>
            {renderUserArr}
          </Select>
        </Form.Item>

        <Form.Item>
          <Button className="bg-blue-500" htmlType="submit">
            ADD USER
          </Button>
        </Form.Item>
      </Form>
      <p className="italic">List of members in the project:</p>
      <div className="text-left px-5 ">{renderuserArrs}</div>
      <div className="text-left  w-full my-5">
        <NewTask />
      </div>
    </div>
  );
}
