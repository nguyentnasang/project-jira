import React from "react";
import { Button } from "antd";
import { NavLink } from "react-router-dom";
import ListProject from "./ListProject";
import { userLocalService } from "../../service/localService";
import Header from "../../component/Header";

export default function HomePage() {
  return (
    <div className="p-20">
      <ListProject />
    </div>
  );
}
