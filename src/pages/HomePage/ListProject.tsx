import { useEffect, useState } from "react";
import { message, Table } from "antd";
import type { ColumnsType } from "antd/es/table";
import { deleteProj, getAllProject } from "./../../service/projectService";
import { NavLink } from "react-router-dom";
import { DeleteOutlined, ToolOutlined } from "@ant-design/icons";
interface DataType {
  //   key: string;
  id: number;
  projectName: string;
  categoryName: string;
  creator: {
    id: number;
    name: string;
  };
  members: any[];
  action?: any;
}

const columns: ColumnsType<DataType> = [
  {
    title: <div className="text-xl uppercase">id</div>,
    dataIndex: "id",
    key: "id",
  },
  {
    title: <div className="text-xl uppercase">projectName</div>,
    dataIndex: "projectNamee",
    key: "projectNamee",
  },

  {
    title: <div className="text-xl uppercase">categoryName</div>,
    dataIndex: "categoryName",
    key: "categoryName",
  },

  {
    title: <div className="text-xl uppercase">creators</div>,
    dataIndex: "creators",
    key: "creators",
  },

  {
    title: <div className="text-xl uppercase">member</div>,
    dataIndex: "member",
    key: "member",
    render: (text) => <i>{text}</i>,
  },

  {
    title: <div className="text-xl uppercase">action</div>,
    dataIndex: "action",
    key: "action",
  },
];

export default function ListProject() {
  let [isload, setIsLoad] = useState(false);
  const [projectArr, setProjectArr] = useState<DataType[]>([]);
  const dataArr = projectArr.map((item: DataType) => {
    return {
      ...item,
      creators: item.creator.name,
      projectNamee: (
        <NavLink to={`/task/${item.id}`}>
          <a className="font-medium hover:brightness-75 text-blue-400 cursor-pointer">
            {item.projectName}
          </a>
        </NavLink>
      ),
      action: (
        <div className="grid grid-cols-2">
          <button
            onClick={() => {
              deleteProj(item.id)
                .then((res) => {
                  message.success("xóa thành công");
                  setIsLoad(!isload);
                })
                .catch((err) => {
                  console.log(err);
                  message.error("xóa thất bại");
                });
            }}
            className="flex justify-center items-center bg-red-500 rounded-md mr-1"
          >
            <DeleteOutlined className="text-white" />
          </button>

          <NavLink
            className=" flex text-white justify-center items-center bg-blue-500 rounded-md"
            to={`/updateproject/${item.id}`}
          >
            <button>
              <ToolOutlined />
            </button>
          </NavLink>
        </div>
      ),
      member: item.members
        .map((member: any) => {
          return member.name;
        })
        .join(", "),
    };
  });
  // item.members.map((member: any) => member.name).join(", ")
  useEffect(() => {
    getAllProject()
      .then((res) => {
        console.log("res", res);
        setProjectArr(res.data.content as DataType[]);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [isload]);

  return (
    <div>
      <Table bordered={true} columns={columns} dataSource={dataArr} />
    </div>
  );
}
