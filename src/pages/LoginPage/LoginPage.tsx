import React, { useState } from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postSignin } from "../../service/userService";
import { setUserInfo } from "../../redux-toolkit/userSlice";
import { useDispatch } from "react-redux/es/exports";
import { userLocalService } from "../../service/localService";
import { NavLink } from "react-router-dom";
export default function LoginPage() {
  // let dispatch = useDispatch();
  const onFinish = (values: any) => {
    console.log("Success:", values);
    postSignin(values)
      .then((res) => {
        message.success("Chào mừng bạn đến với JIRA");
        userLocalService.set(res.data.content);
        setTimeout(() => {
          window.location.href = "/home";
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("thông tin tài khoản hoặc mật khẩu chưa chính xác");
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
    message.warning("hãy nhập đầy đủ thông tin");
  };

  return (
    <div className="bg-red-400 h-screen flex flex-col items-center justify-center">
      <div className="text-4xl font-medium mb-2 animate-pulse">LOG IN</div>
      <Form
        className="w-2/3 bg-white p-10 rounded-xl"
        layout="vertical"
        name="basic"
        // labelCol={{ span: 8 }}
        // wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="EMAIL"
          name="email"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="PASSWORD"
          name="passWord"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>
        <NavLink to={"signup"}>
          <i className="font-normal cursor-pointer hover:text-gray-500">
            I do not have an account?
          </i>
        </NavLink>
        <Form.Item className="mt-5">
          <Button className="bg-blue-500" htmlType="submit">
            LOGIN
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
