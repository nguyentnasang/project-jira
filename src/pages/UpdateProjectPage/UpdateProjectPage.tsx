import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import {
  getProjectDetail,
  putUpdateProject,
} from "../../service/projectService";
import { Button, Form, Input, message, Select } from "antd";
import { Editor } from "@tinymce/tinymce-react";

export default function UpdateProjectPage() {
  let { id } = useParams();
  const { Option } = Select;
  const onFinish = (values: any) => {
    let creatorName = "1";
    if (values.categoryId === "Dự án web") {
      creatorName = "1";
    } else if (values.categoryId === "Dự án phần mềm") {
      creatorName = "2";
    } else if (values.categoryId === "Dự án di động") {
      creatorName = "3";
    } else creatorName = values.categoryId;
    console.log("Success:", values);
    let dataArr = {
      ...values,
      categoryId: creatorName,
      description: description,
    };
    console.log("dataArr", dataArr);
    putUpdateProject(dataArr, id)
      .then((res) => {
        console.log(res);
        message.success("sửa thành công");
      })
      .catch((err) => {
        console.log(err);
        message.warning(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  const [projectDetail, setProjectDetail] = useState({
    id: 0,
    projectName: "",
    creator: { id: 0, name: "" },
    projectCategory: { id: 1, name: "" },
    description: "",
  });
  console.log("projectDetail", projectDetail);
  useEffect(() => {
    getProjectDetail(id)
      .then((res) => {
        console.log(res);
        setProjectDetail(res.data.content);
      })
      .catch((err) => {
        // console.log(err.data.content);
      });
  }, []);
  const [description, setDescription] = useState("");
  const handleEditorChange = (content: any, editor: any) => {
    setDescription(content);
  };
  return (
    <div className="bg-slate-400 flex justify-center items-center py-10 flex-col">
      <div className="text-3xl text-white font-medium mb-2 animate-pulse">
        UPDATE PROJECT
      </div>
      <Form
        layout="vertical"
        className="bg-white w-full p-10 rounded-xl"
        fields={[
          {
            name: ["id"],
            value: projectDetail.id,
          },
          {
            name: ["projectName"],
            value: projectDetail.projectName,
          },
          {
            name: ["creator"],
            value: projectDetail.creator.id,
          },
          {
            name: ["categoryId"],
            value: projectDetail.projectCategory.name,
          },
        ]}
        name="basic"
        style={{ maxWidth: 900 }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item label="Project ID" name="id">
          <Input disabled />
        </Form.Item>
        <Form.Item
          label="Project name "
          name="projectName"
          rules={[{ message: "Please input your projectName!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="creator "
          name="creator"
          //   rules={[{ message: "Please input your projectName!" }]}
        >
          <Input disabled />
        </Form.Item>
        <Form.Item name="categoryId" label="categoryId">
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            <Option value="1">Dự án web</Option>
            <Option value="2">Dự án phần mềm</Option>
            <Option value="3">Dự án di động</Option>
          </Select>
        </Form.Item>

        <Form.Item label="Description" name="description">
          <Editor
            apiKey="YOUR_API_KEY"
            initialValue={projectDetail.description}
            init={{
              height: 200,
              menubar: false,
              plugins: [
                "advlist",
                "autolink",
                "lists",
                "link",
                "image",
                "charmap",
                "preview",
                "anchor",
                "searchreplace",
                "visualblocks",
                "code",
                "fullscreen",
                "insertdatetime",
                "media",
                "table",
                "code",
                "help",
                "wordcount",
              ],
              toolbar:
                "undo redo | formatselect | bold italic backcolor | \
          alignleft aligncenter alignright alignjustify | \
          bullist numlist outdent indent | removeformat | help",
            }}
            onEditorChange={handleEditorChange}
          />
        </Form.Item>
        <Form.Item>
          <NavLink to={"/home"}>
            <Button>CANCEL</Button>
          </NavLink>
          <Button className="bg-blue-500 ml-5" htmlType="submit">
            UPDATE
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
