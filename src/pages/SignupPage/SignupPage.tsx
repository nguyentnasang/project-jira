import React, { useEffect } from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { postSignup } from "./../../service/userService";
import { NavLink } from "react-router-dom";
export default function SignupPage() {
  const onFinish = (values: any) => {
    console.log("Success:", values);
    postSignup(values)
      //arguments, but got 0
      .then((res: any) => {
        console.log(res);
        message.success("đăng ký thành công");
      })
      .catch((err: any) => {
        console.log(err);
        message.error("đăng ký thất bại");
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
    message.warning("hãy nhập đầy đủ thông tin");
  };

  return (
    <div className="bg-red-400 h-screen flex justify-center items-center flex-col">
      <div className="text-4xl font-medium mb-2 animate-pulse">SIGN UP</div>
      <Form
        layout="vertical"
        className="bg-white w-3/4 rounded-xl p-10"
        name="basic"
        style={{ maxWidth: 900 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="EMAIL"
          name="email"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="PASSWORD"
          name="passWord"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="NAME"
          name="name"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input />
        </Form.Item>{" "}
        <Form.Item
          label="PHONE"
          name="phoneNumber"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input />
        </Form.Item>
        <NavLink to={"/"}>
          <i className="font-normal cursor-pointer hover:text-gray-500">
            I already have an account
          </i>
        </NavLink>
        <Form.Item className="mt-5 ">
          <Button className="bg-blue-500 uppercase" htmlType="submit">
            sign up
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
