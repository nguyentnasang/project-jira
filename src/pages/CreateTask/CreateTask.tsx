import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  getAllProject,
  getStatus,
  getTaskType,
  postCreareTask,
} from "../../service/projectService";
import { Button, Checkbox, Form, Input, message, Select } from "antd";
import { getPriority } from "./../../service/projectService";
import { getUserbyProject } from "../../service/userService";
import { useSelector } from "react-redux";
import { Editor } from "@tinymce/tinymce-react";
const { Option } = Select;
export default function CreateTask() {
  const onFinish = (values: any) => {
    console.log("values", values);
    let newData = {
      ...values,
      description: description,
      originalEstimate: values.originalEstimate * 1,
      timeTrackingRemaining: values.timeTrackingRemaining * 1,
      timeTrackingSpent: values.timeTrackingSpent * 1,
    };
    console.log("newData", newData);
    postCreareTask(newData)
      .then((res) => {
        console.log(res);
        message.success("thêm task thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("thêm stask thất bại");
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  let { id } = useParams();
  let idProj = useSelector((state: any) => {
    return state.userSlice.idProj;
  });

  var [description, setDescription] = useState("");
  const handleEditorChange = (content: any) => {
    setDescription(content);
  };

  let [statusArr, setStatusArr] = useState([{ statusName: "", statusId: 0 }]);
  let [priorityArr, setPriorityArr] = useState([
    { priorityId: "", priority: "" },
  ]);
  let [taskType, setTaskType] = useState([{ id: 0, taskType: "" }]);
  let [userByProj, setUserByProj] = useState([{ userId: 0, name: "" }]);
  let [allProject, setAllProject] = useState([{ projectName: "", id: 0 }]);
  useEffect(() => {
    getAllProject()
      .then((res) => {
        setAllProject(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getStatus()
      .then((res) => {
        setStatusArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getPriority()
      .then((res) => {
        setPriorityArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getTaskType()
      .then((res) => {
        setTaskType(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getUserbyProject(idProj)
      .then((res) => {
        setUserByProj(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderStatus = statusArr.map((item) => {
    return <Option value={item.statusId}>{item.statusName}</Option>;
  });
  let renderPriority = priorityArr.map((item) => {
    return <Option value={item.priorityId}>{item.priority}</Option>;
  });
  let rendertasktype = taskType.map((item) => {
    return <Option value={item.id}>{item.taskType}</Option>;
  });
  let renderUserByProject = userByProj.map((item) => {
    return (
      <Option key={item.userId} value={item.userId}>
        {item.name}
      </Option>
    );
  });
  let renderProject = allProject.map((item) => {
    return (
      <Option key={item.id} value={item.id}>
        {item.projectName}
      </Option>
    );
  });
  return (
    <div className="bg-slate-400 flex justify-center items-center flex-col py-10">
      {" "}
      <Form
        className="bg-white w-full p-10 rounded-xl"
        layout="vertical"
        name="basic"
        style={{ maxWidth: 1000 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        {/* project name */}
        <Form.Item
          name="projectId"
          label="projectId"
          rules={[{ required: true }]}
        >
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            {renderProject}
          </Select>
        </Form.Item>
        {/* /taskName */}
        <Form.Item
          label="Task Name"
          name="taskName"
          rules={[{ required: true, message: "Please input your!" }]}
        >
          <Input />
        </Form.Item>
        {/* statusId */}
        <Form.Item
          name="statusId"
          label="statusId"
          rules={[{ required: true }]}
        >
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            {renderStatus}
          </Select>
        </Form.Item>
        <div className="grid grid-cols-2">
          {/* Priority  */}
          <Form.Item
            name="priorityId"
            label="priorityId"
            rules={[{ required: true }]}
          >
            <Select
              placeholder="Select a option and change input text above"
              allowClear
            >
              {renderPriority}
            </Select>
          </Form.Item>
          {/* task type  */}
          <Form.Item name="typeId" label="typeId" rules={[{ required: true }]}>
            <Select
              placeholder="Select a option and change input text above"
              allowClear
            >
              {rendertasktype}
            </Select>
          </Form.Item>
          {/* user by project  */}
        </div>
        <Form.Item
          name="listUserAsign"
          label="listUserAsign"
          // rules={[{ required: true }]}
        >
          <Select
            mode="multiple"
            placeholder="Select a option and change input text above"
            allowClear
          >
            {renderUserByProject}
          </Select>
        </Form.Item>
        <div className="grid grid-cols-3">
          <Form.Item
            label="originalEstimate"
            name="originalEstimate"
            rules={[{ required: true, message: "Please input your!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="timeTrackingSpent"
            name="timeTrackingSpent"
            rules={[{ required: true, message: "Please input your!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="timeTrackingRemaining"
            name="timeTrackingRemaining"
            rules={[{ required: true, message: "Please input your!" }]}
          >
            <Input />
          </Form.Item>
        </div>
        <Form.Item label="description" name="description">
          <Editor
            apiKey="YOUR_API_KEY"
            init={{
              height: 200,
              menubar: false,
              plugins: [
                "advlist",
                "autolink",
                "lists",
                "link",
                "image",
                "charmap",
                "preview",
                "anchor",
                "searchreplace",
                "visualblocks",
                "code",
                "fullscreen",
                "insertdatetime",
                "media",
                "table",
                "code",
                "help",
                "wordcount",
              ],
              toolbar:
                "undo redo | formatselect | bold italic backcolor | \
          alignleft aligncenter alignright alignjustify | \
          bullist numlist outdent indent | removeformat | help",
            }}
            onEditorChange={handleEditorChange}
          />
        </Form.Item>
        <Form.Item>
          <Button className="bg-blue-500" htmlType="submit">
            CREATE TASK
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
