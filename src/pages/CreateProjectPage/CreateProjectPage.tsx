import React, { SetStateAction, useState } from "react";
import { Button, Form, Input, message, Select } from "antd";
import { postCreateProject } from "../../service/projectService";
import { userLocalService } from "../../service/localService";
import { Editor } from "@tinymce/tinymce-react/lib/cjs/main/ts/components/Editor";

export default function CreateProjectPage() {
  const [description, setDescription] = useState("");
  const handleEditorChange = (content: any, editor: any) => {
    setDescription(content);
  };
  const onFinish = (values: any) => {
    let dataArr = {
      ...values,
      alias: userLocalService.get().id,
      description: description,
    };
    console.log("dataArr", dataArr);
    postCreateProject(dataArr)
      .then((res) => {
        console.log(res);
        message.success("Thêm thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("thêm thất bại");
      });
  };
  const { Option } = Select;
  const onFinishFailed = (errorInfo: any) => {
    message.warning("vui lòng nhập đầy đủ thông tin");
  };
  return (
    <div className="bg-slate-400 h-screen flex justify-center items-center flex-col">
      <div className="text-3xl text-white font-medium mb-2 animate-pulse">
        CREATE PROJECT
      </div>
      <Form
        className="bg-white p-10 rounded-xl"
        layout="vertical"
        name="basic"
        style={{ maxWidth: 900 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="projectName"
          name="projectName"
          rules={[{ message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item name="categoryId" label="categoryId" rules={[{}]}>
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            <Option value="1">Dự án web</Option>
            <Option value="2">Dự án phần mềm</Option>
            <Option value="3">Dự án di động</Option>
          </Select>
        </Form.Item>

        <Form.Item label="description" name="description">
          <Editor
            apiKey="YOUR_API_KEY"
            initialValue="<p>Trung tâm đào tạo lập trình Cybersoft</p>"
            init={{
              height: 200,
              menubar: false,
              plugins: [
                "advlist",
                "autolink",
                "lists",
                "link",
                "image",
                "charmap",
                "preview",
                "anchor",
                "searchreplace",
                "visualblocks",
                "code",
                "fullscreen",
                "insertdatetime",
                "media",
                "table",
                "code",
                "help",
                "wordcount",
              ],
              toolbar:
                "undo redo | formatselect | bold italic backcolor | \
          alignleft aligncenter alignright alignjustify | \
          bullist numlist outdent indent | removeformat | help",
            }}
            onEditorChange={handleEditorChange}
          />
        </Form.Item>

        <Form.Item>
          <Button className="bg-blue-500 uppercase" htmlType="submit">
            create propject
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
