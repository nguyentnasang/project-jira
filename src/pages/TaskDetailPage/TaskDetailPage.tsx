import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import {
  getStatus,
  getTaskDetail,
  getTaskType,
  postUpdateTask,
} from "../../service/projectService";
import { Button, Checkbox, Form, Input, message, Select } from "antd";
import { getPriority } from "./../../service/projectService";
import { getUserbyProject } from "../../service/userService";
import { useSelector } from "react-redux";
import { Editor } from "@tinymce/tinymce-react";
import { desRipTionLocal } from "./../../service/localService";
import { idProjLocalService } from "../../service/localService";

const { Option } = Select;
export default function TaskDetailPage() {
  const onFinish = (values: any) => {
    let newDataArr = {
      ...values,
      description: description,
      projectId: parseInt(idProj),
      taskId: id,
    };
    postUpdateTask(newDataArr)
      .then((res) => {
        console.log(res);
        message.success("update thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("update thất bại");
      });
    console.log("newDataArr", newDataArr);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  let { id } = useParams();
  let idProj = useSelector((state: any) => {
    return state.userSlice.idProj;
  });

  let [DataArr, setDataArr] = useState({
    taskName: "",
    statusId: "",
    priorityId: 0,
    typeId: 0,
    assigness: [{ name: "", id: 0 }],
    originalEstimate: 0,
    timeTrackingSpent: 0,
    timeTrackingRemaining: 0,
    description: `${desRipTionLocal.get()}`,
  });

  var [description, setDescription] = useState(`${DataArr.description}`);
  console.log("DataArr.description", DataArr);
  const handleEditorChange = (content: any) => {
    setDescription(content);
    desRipTionLocal.set(content);
  };
  console.log("DataArr", DataArr);

  let [statusArr, setStatusArr] = useState([{ statusName: "", statusId: 0 }]);
  let [priorityArr, setPriorityArr] = useState([
    { priorityId: "", priority: "" },
  ]);
  let [taskType, setTaskType] = useState([{ id: 0, taskType: "" }]);
  let [userByProj, setUserByProj] = useState([{ userId: 0, name: "" }]);
  useEffect(() => {
    getTaskDetail(id)
      .then((res) => {
        setDataArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getStatus()
      .then((res) => {
        setStatusArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getPriority()
      .then((res) => {
        setPriorityArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getTaskType()
      .then((res) => {
        setTaskType(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    getUserbyProject(idProj)
      .then((res) => {
        setUserByProj(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderStatus = statusArr.map((item) => {
    return <Option value={item.statusId}>{item.statusName}</Option>;
  });
  let renderPriority = priorityArr.map((item) => {
    return <Option value={item.priorityId}>{item.priority}</Option>;
  });
  let rendertasktype = taskType.map((item) => {
    return <Option value={item.id}>{item.taskType}</Option>;
  });
  let renderUserByProject = userByProj.map((item) => {
    return (
      <Option key={item.userId} value={item.userId}>
        {item.name}
      </Option>
    );
  });

  return (
    <div className="bg-slate-400 py-10 flex justify-center items-center flex-col">
      <div className="text-4xl text-white font-medium mb-2 animate-pulse">
        UPDATE TASK
      </div>{" "}
      <Form
        className="bg-white w-full p-10 rounded-xl"
        layout="vertical"
        name="basic"
        style={{ maxWidth: 800 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        fields={[
          {
            name: "taskName",
            value: DataArr.taskName,
          },
          {
            name: "statusId",
            value: DataArr.statusId,
          },
          {
            name: "priorityId",
            value: DataArr.priorityId,
          },
          {
            name: "typeId",
            value: DataArr.typeId,
          },
          {
            name: "listUserAsign",
            value: DataArr.assigness.map((item) => {
              return item.id;
            }),
          },
          {
            name: "originalEstimate",
            value: DataArr.originalEstimate,
          },
          {
            name: "timeTrackingSpent",
            value: DataArr.timeTrackingSpent,
          },
          {
            name: "timeTrackingRemaining",
            value: DataArr.timeTrackingRemaining,
          },
        ]}
      >
        {/* /taskName */}
        <Form.Item label="Task Name" name="taskName">
          <Input />
        </Form.Item>
        {/* statusId */}
        <Form.Item name="statusId" label="statusId">
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            {renderStatus}
          </Select>
        </Form.Item>
        <div className="grid grid-cols-2 gap-5">
          {/* Priority  */}
          <Form.Item name="priorityId" label="priorityId">
            <Select
              placeholder="Select a option and change input text above"
              allowClear
            >
              {renderPriority}
            </Select>
          </Form.Item>
          {/* task type  */}
          <Form.Item name="typeId" label="typeId">
            <Select
              placeholder="Select a option and change input text above"
              allowClear
            >
              {rendertasktype}
            </Select>
          </Form.Item>
          {/* user by project  */}
        </div>
        <Form.Item name="listUserAsign" label="listUserAsign">
          <Select
            mode="multiple"
            placeholder="Select a option and change input text above"
            allowClear
          >
            {renderUserByProject}
          </Select>
        </Form.Item>
        <div className="grid grid-cols-3 gap-5">
          <Form.Item label="originalEstimate" name="originalEstimate">
            <Input />
          </Form.Item>
          <Form.Item label="timeTrackingSpent" name="timeTrackingSpent">
            <Input />
          </Form.Item>
          <Form.Item label="timeTrackingRemaining" name="timeTrackingRemaining">
            <Input />
          </Form.Item>
        </div>
        <Form.Item label="description" name="description">
          <Editor
            apiKey="YOUR_API_KEY"
            initialValue={DataArr.description}
            init={{
              height: 200,
              menubar: false,
              plugins: [
                "advlist",
                "autolink",
                "lists",
                "link",
                "image",
                "charmap",
                "preview",
                "anchor",
                "searchreplace",
                "visualblocks",
                "code",
                "fullscreen",
                "insertdatetime",
                "media",
                "table",
                "code",
                "help",
                "wordcount",
              ],
              toolbar:
                "undo redo | formatselect | bold italic backcolor | \
          alignleft aligncenter alignright alignjustify | \
          bullist numlist outdent indent | removeformat | help",
            }}
            onEditorChange={handleEditorChange}
          />
        </Form.Item>
        <Form.Item>
          <NavLink to={`/task/${idProjLocalService.get() * 1}`}>
            <Button>CANCEL</Button>
          </NavLink>
          <Button className="bg-blue-500 ml-5" htmlType="submit">
            UPDATE
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
