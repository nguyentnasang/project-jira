import React from "react";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage/LoginPage";
import SignupPage from "./pages/SignupPage/SignupPage";
import HomePage from "./pages/HomePage/HomePage";
import CreateProjectPage from "./pages/CreateProjectPage/CreateProjectPage";
import UpdateProjectPage from "./pages/UpdateProjectPage/UpdateProjectPage";
import TaskPage from "./pages/TaskPage/TaskPage";
import TaskDetailPage from "./pages/TaskDetailPage/TaskDetailPage";
import CreateTask from "./pages/CreateTask/CreateTask";
import Layout from "./HOC/Layout";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/signup" element={<SignupPage />} />
          <Route
            path="/home"
            element={
              <Layout>
                <HomePage />
              </Layout>
            }
          />
          <Route
            path="/createproject"
            element={
              <Layout>
                <CreateProjectPage />
              </Layout>
            }
          />
          <Route
            path="/updateproject/:id"
            element={
              <Layout>
                <UpdateProjectPage />
              </Layout>
            }
          />
          <Route
            path="/task/:id"
            element={
              <Layout>
                <TaskPage />
              </Layout>
            }
          />
          <Route
            path="/taskdetail/:id"
            element={
              <Layout>
                <TaskDetailPage />
              </Layout>
            }
          />
          <Route
            path="/createtask"
            element={
              <Layout>
                <CreateTask />
              </Layout>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
