import { https } from "./configURL";

//Đăng ký
export const postSignup = (data: any) => {
  return https.post("/api/Users/signup", data);
};
//Đăng nhập
export const postSignin = (data: any) => {
  return https.post("/api/Users/signin", data);
};
//lấy user theo project
export const getUserbyProject = (id: number) => {
  return https.get(`api/Users/getUserByProjectId?idProject=${id}`);
};
//lấy user
export const getUser = () => {
  return https.get(`/api/Users/getUser`);
};
//thêm user vào project
export const postAddUserProj = (data: any) => {
  return https.post(`/api/Project/assignUserProject`, data);
};
//xóa user từ Project
export const postRemoteUserProj = (data: any) => {
  return https.post(`/api/Project/removeUserFromProject`, data);
};
