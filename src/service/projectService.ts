import { https } from "./configURL";

//lấy danh sách project
export const getAllProject = () => {
  return https.get("/api/Project/getAllProject");
};
//Thêm dự án
export const postCreateProject = (data: any) => {
  return https.post("/api/Project/createProjectAuthorize", data);
};
//Lấy thông tin chi tiết phim
export const getProjectDetail = (id: any) => {
  return https.get(`api/Project/getProjectDetail?id=${id}`);
};
// cập nhật project
export const putUpdateProject = (data: any, id: any) => {
  return https.put(`api/Project/updateProject?projectId=${id}`, data);
};
// lấy chi tiết nhiệm vụ
export const getTaskDetail = (id: any) => {
  return https.get(`api/Project/getTaskDetail?taskId=${id}`);
};
//lấy danh sách giá trị status
export const getStatus = () => {
  return https("/api/Status/getAll");
};
//lấy danh sách Priority(sự ưu tiên)
export const getPriority = () => {
  return https.get("/api/Priority/getAll");
};
//lấy danh sách TaskType
export const getTaskType = () => {
  return https.get("/api/TaskType/getAll");
};
//cập nhật stask
export const postUpdateTask = (data: any) => {
  return https.post("/api/Project/updateTask", data);
};
//lấy tất cả project
export const getAllProj = () => {
  return https.post("/api/getAllProject");
};
// thêm stask
export const postCreareTask = (data: any) => {
  return https.post("/api/Project/createTask", data);
};
// xóa project

export const deleteProj = (id: number) => {
  return https.delete(`/api/Project/deleteProject?projectId=${id}`);
};
//xóa task
export const deleteTask = (id: number) => {
  return https.delete(`/api/Project/removeTask?taskId=${id}`);
};
