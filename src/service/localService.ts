export const USERLOCAL: string = "USERLOCAL";
export const IDPROJECT: string = "IDPROJECT";
export const DES: string = "DES";
export const userLocalService = {
  get: () => {
    let userJson = localStorage.getItem(USERLOCAL);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  set: (data: any) => {
    let userJson = JSON.stringify(data);
    localStorage.setItem(USERLOCAL, userJson);
  },
  remote: () => {
    localStorage.removeItem(USERLOCAL);
  },
};
export const idProjLocalService = {
  get: () => {
    let userJson = localStorage.getItem(IDPROJECT);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  set: (data: any) => {
    let userJson = JSON.stringify(data);
    localStorage.setItem(IDPROJECT, userJson);
  },
  remote: () => {
    localStorage.removeItem(IDPROJECT);
  },
};
export const desRipTionLocal = {
  get: () => {
    let userJson = localStorage.getItem(DES);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  set: (data: any) => {
    let userJson = JSON.stringify(data);
    localStorage.setItem(DES, userJson);
  },
  remote: () => {
    localStorage.removeItem(DES);
  },
};
